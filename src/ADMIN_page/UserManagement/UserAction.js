import { message } from "antd";
import React from "react";
import { userServ } from '../../Services/userServ';

export default function UserAction({ taiKhoan, onSuccess }) {
  let handleDeleteUser = () => {
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xoá user thành công");
        console.log(res);
        onSuccess();
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
  return (
    <div className="space-x-2">
      <button
        onClick={handleDeleteUser}
        className="px-5 py-2 rounded bg-red-500 text-white"
      >
        Xoá
      </button>
      <button
        className="
       px-5 py-2 rounded bg-blue-500 text-white
      
      "
      >
        Sửa
      </button>
    </div>
  );
}
