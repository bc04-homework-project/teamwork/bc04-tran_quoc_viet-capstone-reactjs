import React, { useEffect, useState } from "react";
import UserAction from "./UserAction";
import UserTable from "./UserTable";
import { userServ } from '../../Services/userServ';

export default function UserManagement() {
  const [userList, setUserList] = useState([]);
  useEffect(() => {
    let fetchUserList = () => {
      userServ
        .getUserList()
        .then((res) => {
          let data = res.data.content.map((item) => {
            return {
              ...item,
              action: (
                <UserAction
                  onSuccess={fetchUserList}
                  taiKhoan={item.taiKhoan}
                />
              ),
            };
          });
          setUserList(data);
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchUserList();
  }, []);
  return (
    <div className="container mx-auto pt-20">
      <UserTable userList={userList} />
    </div>
  );
}
