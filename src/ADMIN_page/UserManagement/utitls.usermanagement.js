import { Tag } from "antd";

export const headColumns = [
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Tên khách hàng",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Loại tài khoản",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",

    render: (text) => {
      if (text == "QuanTri") {
        return <Tag color="volcano">Quản Trị</Tag>;
      } else {
        return <Tag color="green">Khách hàng</Tag>;
      }
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

