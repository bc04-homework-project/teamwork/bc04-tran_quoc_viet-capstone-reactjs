import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./USER_page/Pages/HomePage/HomePage";
import DetailMoviePage from "./USER_page/Pages/DetailMoviePage/DetailMoviePage";
import LoginPage from "./USER_page/Pages/LoginPage/LoginPage";
import Spinner from "./USER_page/Components/Spinner/Spinner";
import TicketBookingPage from './USER_page/Pages/TicketBookingPage/TicketBookingPage';
import Layout from "./USER_page/HOC/Layout/Layout";
import SecureView from "./USER_page/HOC/SecureView/SecureView";
import UserManagement from './ADMIN_page/UserManagement/UserManagement';

function App() {
  return (
    <div className="">
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} /> } />
          <Route path="/detail/:id" element={<Layout Component={DetailMoviePage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/booking/:id" element={<SecureView><TicketBookingPage /></SecureView>} />
          <Route path="/admin/userManagement" element={<SecureView><Layout Component={UserManagement} /></SecureView>} />

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
