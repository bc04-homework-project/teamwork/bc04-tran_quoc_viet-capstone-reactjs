import { https } from "./configURL";

export const moviesServ = {
  getListMovie: () => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP11";
    return https.get(uri);
  },

  getMovieDetail: (id) =>
    https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`),
  
    getShowTimeByMovie: (id) =>
    https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`),

  getMovieBanners: () => https.get(`/api/QuanLyPhim/LayDanhSachBanner`),

  getMovieByTheater: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP13"
    );
  },

  getBookingInfo: (id) =>
    https.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${id}`),
};

