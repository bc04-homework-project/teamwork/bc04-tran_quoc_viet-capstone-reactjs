import { https } from "./configURL";
import { localServ } from "./localServ";

export const userServ = {
  postLogin: (dataLogin) =>
    https.post(`/api/QuanLyNguoiDung/DangNhap`, dataLogin),

  getUserList: () =>
    https.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${
        localServ.user.get()?.maNhom
      }`
    ),

  deleteUser: (taiKhoan) =>
    https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`),
};
