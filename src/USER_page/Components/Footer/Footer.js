import React from "react";

export default function Footer() {
  return (
    <div>
      <footer className="p-4 md:p-8 lg:p-10 bg-gray-800">
        <div className="mx-auto max-w-screen-xl text-center">
          <a
            href="/"
            className="flex justify-center items-center text-2xl font-semibold text-gray-900 dark:text-white"
          >
            Amazzzing Flix
          </a>
          <p className="my-6 text-gray-500 dark:text-gray-400">
            Let Movies be with you!
          </p>
          <ul className="flex flex-wrap justify-center items-center mb-6 text-gray-900 dark:text-white">
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6 ">
                About
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6">
                Premium
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6 ">
                Campaigns
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6">
                Blog
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6">
                Affiliate Program
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6">
                FAQs
              </a>
            </li>
            <li>
              <a href="#" className="mr-4 hover:underline md:mr-6">
                Contact
              </a>
            </li>
          </ul>
          <p className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
            <span className="mr-3">© 2022 - infinity </span>
            <a href="/" className="hover:underline">
              Amazzzing Flix™
            </a>
            . All Rights Reserved.
          </p>
        </div>
      </footer>
    </div>
  );
}
