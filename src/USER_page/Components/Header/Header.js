import React from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { localServ } from "./../../../Services/localServ";
import { useState } from "react";
import { message } from "antd";
import { LogoutOutlined, LoginOutlined } from "@ant-design/icons";

export default function Header() {
  const [user, setUser] = useState(localServ.user.get());
  let navigate = useNavigate();
  let location = useLocation();

  let handleLogout = () => {
    setUser(null);
    localServ.user.remove();
    if (location.pathname.slice(0, 7) == "/bookin" || '/admin/') {
      message.error("Để tiếp tục bạn cần đăng nhập");
      setTimeout(() => {
        navigate("/login");
      }, 3000);
    }
  };

  let handleLogin = () => {
    navigate("/login");
  };

  let renderLogin = () => {
    if (!user) {
      return (
        <p className="">
          <button
            onClick={handleLogin}
            className="px-2 py-1 bg-green-400 hover:bg-green-600 duration-300 text-white rounded"
          >
            <LoginOutlined className="text-xl py-1" /> <span> Log in</span>
          </button>
        </p>
      );
    }
    return (
      <p className="">
        <NavLink to={'/admin/userManagement'}>
        <span className="text-blue-400 mr-3 animate-pulse">{user.hoTen}</span></NavLink>
        <button
          onClick={handleLogout}
          className="px-2 py-1 bg-red-400 hover:bg-red-600 duration-300 text-white rounded "
        >
          <LogoutOutlined className="text-xl py-1" /> <span> Log out</span>
        </button>
      </p>
    );
  };

  return (
    <div className="container bg-gray-800 shadow-xl fixed top-0 w-full z-50">
      <div className="flex justify-between pt-2 px-2">
        <NavLink to={"/"}>
          <div className="hover:animate-spin ">
            <span className="text-green-400 text-3xl">Amazzzing Flix</span>{" "}
          </div>
        </NavLink>

        <div className="w-fit mx-auto">
          <div className="pt-1 text-xl">
            <a className="text-blue-500" href="/">
              Home
            </a>
            <a className="text-white mx-5" href="/#movieTheaters">
              Cụm rạp
            </a>
            <a className="text-white" href="#">
              link
            </a>
          </div>{" "}
        </div>

        <div className="pt-1"> {renderLogin()} </div>
      </div>
    </div>
  );
}
