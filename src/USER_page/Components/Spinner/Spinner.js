import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Spinner() {
  let isLoading = useSelector((state) => state.movieSlice.isLoading);
  return (
    isLoading && (
      <div className="bg-black w-screen h-screen fixed left-0 top-0 flex justify-center items-center z-50">
        <RingLoader size={200} color="#36d7b7" />
      </div>
    )
  );
}
