import React from 'react'
import Header from './../../Components/Header/Header';
import Footer from '../../Components/Footer/Footer';

export default function Layout({Component}) {
  return (
    <div>
        
        <div className='mx-auto container'><Header/></div>
        
        {<Component/>}

        <div className='mx-auto container'><Footer/></div>


    </div>
  )
}
