import React from "react";
import { useEffect } from "react";
import { localServ } from "./../../../Services/localServ";
import { message } from "antd";

export default function SecureView({ children }) {
  useEffect(() => {
    let userLocal = localServ.user.get();
    if (!userLocal) {
      message.error("Để tiếp tục bạn cần đăng nhập");
      setTimeout(() => {
        window.location.href = "/login";
      }, 2000);
    }
  }, []);

  return <div>{children}</div>;
}
