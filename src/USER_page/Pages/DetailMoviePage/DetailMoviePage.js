import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { loadingOff, loadingOn } from "../../../redux/slice/movieSlice";
import { moviesServ } from "../../../Services/movieServ";
import ShowTimeByMovie from "./ShowTimeByMovie";
import "./DetailMovie.css";

export default function DetailMoviePage() {
  const [movie, setMovie] = useState({});

  let dispatch = useDispatch();

  let { id } = useParams();

  useEffect(() => {
    dispatch(loadingOn());

    moviesServ
      .getMovieDetail(id)
      .then((res) => {
        dispatch(loadingOff());
        // console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        dispatch(loadingOff());
        console.log(err);
      });
  }, []);

  return (
    <div
      className="container mx-auto detailMovie"
      style={{ background: `url(${movie.hinhAnh})` }}
    >
      <div className="detailMovie2 pt-16 pb-10">
        <div className="flex pt-3">
          <div className="w-4/12 pl-3 pt-16">
            <h1 className="text-5xl text-blue-300 text-center">
              {movie.tenPhim}
            </h1>
            <p
              className={`px-2 py-1 mx-auto rounded text-white w-fit ${
                movie.dangChieu ? "bg-green-400" : "bg-red-400"
              } `}
            >
              {movie.dangChieu ? "Đang Chiếu" : "Sắp Chiếu"}
            </p>
            <h3 className="text-lg text-white h-60 overflow-auto">
              {movie.moTa}
            </h3>
          </div>

          <div className="w-4/12 h-fit mx-auto text-center hover:scale-125 hover:translate-y-12 duration-1000 shadow-yellow-400">
            <img
              className="mx-auto h-96"
              alt={movie.biDanh}
              src={movie.hinhAnh}
            />
          </div>

          <div className="w-4/12 text-center pt-16">
            <h2 className="text-6xl text-green-300">Rating</h2>
            <h3 className="text-9xl text-red-500 rounded-full w-fit p-5 mx-auto  outline-dotted outline-8 outline-green-500">
              {movie.danhGia}
            </h3>
          </div>
        </div>

        <div className="mt-8">
          <iframe
            className="mx-auto"
            width="560"
            height="315"
            src={movie.trailer}
            title="YouTube video player"
            frameBorder="1"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>

        <div className="w-fit mx-auto">
          <ShowTimeByMovie />
        </div>
      </div>
    </div>
  );
}
