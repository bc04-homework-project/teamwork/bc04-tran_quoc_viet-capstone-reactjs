import { Tabs } from "antd";
import React from "react";
import { moviesServ } from "../../../Services/movieServ";
import { useEffect } from "react";
import { useState } from "react";
import moment from "moment";
import { NavLink } from "react-router-dom";
import { useParams } from "react-router-dom";

export default function ShowTimeByMovie() {
  const [showTime, setShowTime] = useState([]);
  let { id } = useParams();

  useEffect(() => {
    moviesServ
      .getShowTimeByMovie(id)
      .then((res) => {
        setShowTime(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderShowTime = () => {
    //render logo hệ thống rạp
    return showTime.heThongRapChieu?.map((theater, index) => {
      return {
        label: <img src={theater.logo} className=" h-12" />,
        key: index,
        children: (
          <Tabs
            tabPosition={"left"}
            defaultActiveKey="1"
            //render tên rạp và địa chỉ rạp
            items={theater.cumRapChieu.map((cumRap, index2) => {
              return {
                label: (
                  <div className="text-left w-64 ">
                    <p className="text-red-400">{cumRap.tenCumRap}</p>
                    <p className="text-xs">{cumRap.diaChi}</p>
                  </div>
                ),
                key: index2,
                //render các suất chiếu phim
                children: (
                  <div className="grid grid-cols-3 gap-2 w-fit">
                    {cumRap.lichChieuPhim.map((lichChieu, index3) => {
                      return (
                        <NavLink
                          key={index3}
                          to={`/booking/${lichChieu.maLichChieu}`}
                        >
                          <p
                            className={`${
                              showTime.dangChieu ? "bg-green-600 hover:bg-green-400" : "bg-red-600 hover:bg-red-400"
                            } px-2 py-1 text-white duration-300`}
                          >
                            {moment(lichChieu.ngayChieuGioChieu).format(
                              "dd/mm/yyy - hh:mm A"
                            )}
                          </p>
                        </NavLink>
                      );
                    })}{" "}
                  </div>
                ),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <div
      className="mt-10 overflow-y-auto border border-blue-300"
      style={{ height: "60vh" }}
    >
      <Tabs
        className=""
        tabPosition={"left"}
        defaultActiveKey="1"
        items={renderShowTime()}
      />
    </div>
  );
}

/**{
  "statusCode": 200,
  "message": "Xử lý thành công!",
  "content": {
    "maPhim": 1521,
    "tenPhim": "Spectre 22",
    "biDanh": "spectre-22",
    "trailer": "https://www.youtube.com/embed/LTDaET-JweU",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/specter1123_gp13.jpg",
    "moTa": "A cryptic message from Bond's past sends him on a trail to uncover a sinister organization. While M battles political forces to keep the secret service alive, Bond peels back the layers of deceit to reveal the terrible truth behind SPECTER.",
    "maNhom": "GP13",
    "hot": true,
    "dangChieu": true,
    "sapChieu": false,
    "ngayKhoiChieu": "2022-09-14T11:33:07.42",
    "danhGia": 6
  },
  "dateTime": "2022-10-01T18:54:24.6737353+07:00",
  "messageConstants": null
} */
