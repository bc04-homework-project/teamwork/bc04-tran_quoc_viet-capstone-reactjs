import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { moviesServ } from "./../../../Services/movieServ";

const contentStyle = {
  width: "100%",
  height: "50vh",
  color: "#fff",
  textAlign: "center",
  background: "black",
  backgroundSize: "100%",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
};

export default function CarouselHomePage() {
  const [movieBanners, setMovieBanners] = useState([{
    "maBanner": 1,
    "maPhim": 1282,
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png"
  }]);

  useEffect(() => {
    moviesServ
      .getMovieBanners()
      .then((res) => {
        setMovieBanners(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderMovieBanners = () => {
    return movieBanners.map((banner) => {
      return (
        <div key={banner.maPhim}>
          <div
            style={{
              ...contentStyle,
              backgroundImage: `url(${banner.hinhAnh})`,
            }}
          >
            <img
              src={banner.hinhAnh}
              style={contentStyle}
              className="w-full opacity-0"
              alt={banner.hinhAnh}
            />
          </div>
        </div>
      );
    });
  };

  return (
    <div className="mb-5">
      <Carousel autoplay>{renderMovieBanners()}</Carousel>
    </div>
  );
}
