import React from "react";
import { useState } from "react";

export default function FilterMovie({ movieList, setMovieListToShow }) {
    
  const activeClass = { dangChieu: false, sapChieu: false, allMovie: true };
  const [activeButton, setActiveButton] = useState(activeClass);

  let handleDangChieu = () => {
    let activeClass2 = { ...activeClass, dangChieu: true, allMovie: false };
    setActiveButton(activeClass2);
    activeClass2.dangChieu
      ? setMovieListToShow(movieList.filter((movie) => movie.dangChieu == true))
      : setMovieListToShow(movieList);
  };

  let handleSapChieu = () => {
    let activeClass2 = { ...activeClass, sapChieu: true, allMovie: false };
    setActiveButton(activeClass2);
    activeClass2.sapChieu
      ? setMovieListToShow(movieList.filter((movie) => movie.sapChieu == true))
      : setMovieListToShow(movieList);
  };

  let handleAllMovie = () => {
    setActiveButton(activeClass);
    setMovieListToShow(movieList);
  };

  return (
    <div>
      <button
        onClick={handleAllMovie}
        className={`px-4 py-2 border bg-red-300 hover:bg-red-500 duration-300 border-red-500 rounded-xl mr-2 ${
          activeButton.allMovie ? "bg-white" : ""
        }`}
      >
        Tất cả phim
      </button>
      <button
        onClick={handleDangChieu}
        className={`px-4 py-2 border bg-red-300 hover:bg-red-500 duration-300 border-red-500 rounded-xl mr-2 ${
          activeButton.dangChieu ? "bg-white" : ""
        }`}
      >
        Đang Chiếu
      </button>
      <button
        onClick={handleSapChieu}
        className={`px-4 py-2 border bg-red-300 hover:bg-red-500 duration-300 border-red-500 rounded-xl mr-2 ${
          activeButton.sapChieu ? "bg-white" : ""
        }`}
      >
        Sắp Chiếu
      </button>
    </div>
  );
}
