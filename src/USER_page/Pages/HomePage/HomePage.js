import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { loadingOff, loadingOn } from "../../../redux/slice/movieSlice";
import { moviesServ } from "../../../Services/movieServ";
import CarouselHomePage from "./CarouselHomePage";
import FilterMovie from "./FilterMovie";
import MovieList from "./MovieList";
import MovieTheaters from "./MovieTheaters";

export default function HomePage() {
  const [movieList, setMovieList] = useState([]);
  let dispatch = useDispatch();
  const [movieListToShow, setMovieListToShow] = useState(movieList);

  useEffect(() => {
    dispatch(loadingOn());
    moviesServ
      .getListMovie()
      .then((res) => {
        setMovieList(res.data.content);
        setMovieListToShow(res.data.content);
        dispatch(loadingOff());
      })
      .catch((err) => {
        dispatch(loadingOff());
        console.log(err);
      });
  }, []);

  return (
    <div className="container mx-auto pb-10 bg-gray-200">
      <div className="pt-14">
      <div>
        <CarouselHomePage />
      </div>

      <div className="mx-auto w-fit">
        <FilterMovie
          movieList={movieList}
          setMovieListToShow={setMovieListToShow}
        />

        <MovieList films={movieListToShow} />
      </div>

      <div className="mx-auto w-fit hover:shadow-2xl duration-300">
        <MovieTheaters />
      </div></div>
    </div>
  );
}
