import React from "react";
import { NavLink } from "react-router-dom";
import { Card, Pagination } from "antd";
import { useState } from "react";

export default function MovieList({ films }) {
  let numberOfItem = 10;

  const [minIndex, setMinIndex] = useState(0);
  const [maxIndex, setMaxIndex] = useState(numberOfItem);

  let renderMovieList = () => {
    return films.map((movie, index) => {
      return (
        index >= minIndex &&
        index < maxIndex && (
          <NavLink 
            key={movie.maPhim}
            className="w-44 border hover:border-red-300 duration-300 mx-auto"
            to={`/detail/${movie.maPhim}`}
          >
            <Card
              className="text-center w-full h-full "
              hoverable
              cover={<img className="h-64" src={movie.hinhAnh} />}
            >
              <p className="text-lg text-red-400 truncate">{movie.tenPhim}</p>
              <p>
                {movie.moTa.length > 80 ? (
                  <span>{movie.moTa.slice(0, 80)} ...</span>
                ) : (
                  <span>{movie.moTa}</span>
                )}
              </p>
            </Card>
          </NavLink>
        )
      );
    });
  };

  const onChange = (pageNumber, pageSize) => {
    setMinIndex((pageNumber - 1) * pageSize);
    setMaxIndex(pageNumber * pageSize);
  };

  return (
    <div>
      <div className="grid lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-2 gap-2 mb-5 mt-2">
        {renderMovieList()}
      </div>

      <Pagination
        className="text-center"
        defaultCurrent={1}
        total={films.length}
        showQuickJumper
        showTotal={(total) => `Total ${total} Movies`}
        onChange={onChange}
        pageSize={numberOfItem}
        responsive
      />
    </div>
  );
}
