import { Tabs } from "antd";
import React from "react";
import { moviesServ } from "../../../Services/movieServ";
import { useEffect } from "react";
import { useState } from "react";
import moment from "moment";
import { NavLink } from 'react-router-dom';

export default function MovieTheaters() {
  const [movieListByTheater, setMovieListByTheater] = useState([]);

  useEffect(() => {
    moviesServ
      .getMovieByTheater()
      .then((res) => {
        setMovieListByTheater(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let onImgError = (e) => { 
    e.target.onerror = null;
    e.target.src = "https://m.media-amazon.com/images/M/MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_.jpg"
   };

  let renderTheaters = () => {
    //render logo hệ thống rạp
    return movieListByTheater.map((theater, index) => {
      return {
        label: <img src={theater.logo} className=" h-12" />,
        key: index,
        children: (
          <Tabs
            tabPosition={"left"}
            defaultActiveKey="1"
    //render tên rạp và địa chỉ rạp
            items={theater.lstCumRap.map((cumRap, index2) => {
              return {
                label: (
                  <div className="text-left w-64 ">
                    <p className="text-red-400">{cumRap.tenCumRap}</p>
                    <p className="text-xs">{cumRap.diaChi}</p>
                  </div>
                ),
                key: index2,
    //render poster phim và tên phim
                children: cumRap.danhSachPhim.map((phim, index3) => {
                  return (
                    <div key={index3} className="flex my-1 h-44 shadow-sm">
                      <div>
                        <img src={phim.hinhAnh} className="w-28 h-40" onError={(e)=> onImgError(e)}/>
                      </div>
                      <div className="ml-2">
                        <p>
                          <span className="text-xl text-blue-500 mr-2">
                            {phim.tenPhim}
                          </span>
                          {phim.dangChieu ? (
                            <span className="px-2 py-1 bg-green-400 text-white rounded-md">
                              Đang Chiếu
                            </span>
                          ) : (
                            <span className="px-2 py-1 bg-red-300 text-white rounded-md">
                              Sắp Chiếu
                            </span>
                          )}
                        </p>
   {/* render các suất chiếu phim */}
                        <div className="grid grid-cols-3 h-32 overflow-auto">
                          {phim.lstLichChieuTheoPhim?.map((lichChieu, index4) => {
                            return (
                              <NavLink key={index4} to={`/booking/${lichChieu.maLichChieu}`}>
                              <p
                               
                                className={`${phim.dangChieu? "bg-green-600 hover:bg-green-400" : "bg-red-600 hover:bg-red-400"} px-2 py-1 text-white duration-300 mr-2 h-fit`}
                              > 
                               {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                              </p></NavLink>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  );
                }),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <div id="movieTheaters"
      className="mt-10 overflow-y-auto border border-blue-200"
      style={{ height: "60vh" }}
    >
      <Tabs
        className=" "
        tabPosition={"left"}
        defaultActiveKey="1"
        items={renderTheaters()}
      />
    </div>
  );
}
