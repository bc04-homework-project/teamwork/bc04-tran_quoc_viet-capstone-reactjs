import React from "react";
import FormLogin from "./FormLogin";
import "./Login.css";

export default function LoginPage() {
  return (
    <div className="container mx-auto">
      <div className="loginForm container mx-auto">
        <div className="w-fit mx-auto hover:shadow-xl duration-500">
          <FormLogin />
        </div>
      </div>
    </div>
  );
}
