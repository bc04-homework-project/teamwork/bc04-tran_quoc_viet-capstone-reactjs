import _ from "lodash";
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { pickSeat } from "../../../redux/slice/movieSlice";

export default function SeatMap({}) {
  let dispatch = useDispatch();
  let { danhSachGhe } = useSelector(
    (state) => state.movieSlice.movieBookingInfo
  );
  let { selectedSeats } = useSelector((state) => state.movieSlice);

  let renderSeatList = () => {
    return danhSachGhe?.map((seat, index) => {
      let classGheVip = seat.loaiGhe == "Vip" ? "bg-red-300" : "bg-gray-300";
      let classGheDaDat = seat.daDat ? "bg-gray-500" : "";
      let classGheDangChon = "";
      let indexGheDangChon = selectedSeats.findIndex(
        (seat2) => seat2.maGhe == seat.maGhe
      );
      if (indexGheDangChon != -1) {
        classGheDangChon = "gheDangChon";
      }

      let handleSelectSeat = () => {
        dispatch(pickSeat(seat));
      };

      return (
        <Fragment key={index}>
          <button
            onClick={handleSelectSeat}
            disabled={seat.daDat}
            className={`w-8 h-8 rounded m-1  ${
              seat.daDat ? "" : "hover:bg-green-600"
            } duration-300 ${classGheVip} ${classGheDaDat} ${classGheDangChon}`}
          >
            {seat.daDat ? "X" : seat.tenGhe}
          </button>

          {(index + 1) % 16 == 0 && <br />}
        </Fragment>
      );
    });
  };

  return (
    <div>
      <div className="bg-gray-600 text-white text-center text-xl mt-6 mb-14 shadow-xl shadow-green-200">
        Screen
      </div>

      <div className="text-center"> {renderSeatList()} </div>

      <div className="flex justify-center mt-10 bg-blue-100 w-fit mx-auto pt-4 shadow-2xl">
        <div className="w-20">
          <p className="bg-gray-500 w-8 h-8 rounded text-center pt-1 mb-0 mx-auto ">
            X
          </p>
          <p className="text-center">Đã đặt</p>
        </div>
        <div className="w-20">
          <p className="bg-gray-300 w-8 h-8 rounded text-center pt-1 mb-0 mx-auto "></p>
          <p className="text-center">Thường</p>
        </div>
        <div className="w-20">
          <p className="bg-red-300 w-8 h-8 rounded text-center pt-1 mb-0 mx-auto "></p>
          <p className="text-center">Vip</p>
        </div>
        <div className="w-20">
          <p className="gheDangChon w-8 h-8 rounded text-center pt-1 mb-0 mx-auto"></p>
          <p className="text-center">Đang chọn</p>
        </div>
      </div>
    </div>
  );
}
