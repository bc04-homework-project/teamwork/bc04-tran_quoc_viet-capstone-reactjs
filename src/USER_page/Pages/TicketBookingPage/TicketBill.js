import _ from "lodash";
import React from "react";
import { useSelector } from "react-redux";

export default function TicketBill() {
  let { movieBookingInfo, selectedSeats } = useSelector(
    (state) => state.movieSlice
  );

  let renderGheDangChon = () => {
    return _.sortBy(selectedSeats, ["stt"]).map((seat) => {
      return <span key={seat.maGhe}>{seat.tenGhe} , </span>;
    });
  };

  let renderTongTien = () => {
    return selectedSeats
      .reduce((tongTien, seat) => {
        return (tongTien += seat.giaVe);
      }, 0)
      .toLocaleString();
  };

  return (
    <div className="shadow-2xl bg-black p-2">
      <div>
        <img
          src={movieBookingInfo.thongTinPhim?.hinhAnh}
          className="h-72 mx-auto pt-3"
        />
      </div>

      <div className="flex justify-between">
        <p className="text-red-400">Cụm rạp</p>
        <p className="text-white">{movieBookingInfo.thongTinPhim?.tenCumRap}</p>
      </div>
      <hr />
      <div className="flex justify-between">
        <p className="text-red-400">Địa chỉ</p>
        <p className="text-white">{movieBookingInfo.thongTinPhim?.diaChi}</p>
      </div>
      <hr />

      <div className="flex justify-between">
        <p className="text-red-400">Rạp</p>
        <p className="text-white">{movieBookingInfo.thongTinPhim?.tenRap}</p>
      </div>
      <hr />
      <div className="flex justify-between">
        <p className="text-red-400">Thời gian chiếu</p>
        <p className="text-white">
          {movieBookingInfo.thongTinPhim?.ngayChieu} -
          {movieBookingInfo.thongTinPhim?.gioChieu}
        </p>
      </div>
      <hr />

      <div className="flex justify-between">
        <p className="text-red-400">Tên phim</p>
        <p className="text-white">{movieBookingInfo.thongTinPhim?.tenPhim}</p>
      </div>
      <hr />

      <div className="flex justify-between flex-wrap">
        <p className="text-red-400">Ghế chọn</p>
        <p className="text-white">{renderGheDangChon()}</p>
      </div>
      <hr />

      <p className="text-center text-4xl text-green-400">
        
        {renderTongTien()} VND
      </p>

      <div>
        <p className="py-3 bg-green-400 text-center text-white text-4xl cursor-pointer hover:bg-red-500 duration-300 rounded">
          Đặt Vé
        </p>
      </div>
    </div>
  );
}
