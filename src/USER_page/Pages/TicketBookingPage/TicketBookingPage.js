import React from "react";
import SeatMap from "./SeatMap";
import TicketBill from "./TicketBill";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadingOff,
  loadingOn,
  setMovieBookingInfo,
} from "../../../redux/slice/movieSlice";
import { moviesServ } from "./../../../Services/movieServ";
import { useParams } from "react-router-dom";
import "./TicketBooking.css";
import Header from "./../../Components/Header/Header";

export default function TicketBookingPage() {
  let { id } = useParams();
  let dispatch = useDispatch();
  let { movieBookingInfo } = useSelector((state) => state.movieSlice);

  useEffect(() => {
    dispatch(loadingOn());

    moviesServ
      .getBookingInfo(id)
      .then((res) => {
        dispatch(setMovieBookingInfo(res.data.content));
        dispatch(loadingOff());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div
      className="container mx-auto tiketBooking"
      style={{ background: `url(${movieBookingInfo.thongTinPhim?.hinhAnh})` }}
    >
      <div className="mx-auto container">
        <Header />
      </div>

      <div className="flex tiketBooking2 pt-12">
        <div className="w-9/12 min-w-fit">
          <SeatMap />
        </div>

        <div className="w-3/12 ">
          <TicketBill />
        </div>
      </div>
    </div>
  );
}
