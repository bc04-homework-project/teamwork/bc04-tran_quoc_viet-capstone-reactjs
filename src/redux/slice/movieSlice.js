import { createSlice } from "@reduxjs/toolkit";
import _ from "lodash";

const initialState = {
  isLoading: false,
  movieBookingInfo: {},
  selectedSeats: [],
};

const movieSlice = createSlice({
  name: "movieSlice",
  initialState,
  reducers: {
    loadingOn: (state) => {
      state.isLoading = true;
    },
    loadingOff: (state) => {
      state.isLoading = false;
    },
    setMovieBookingInfo: (state, action) => {
      state.movieBookingInfo = action.payload;
    },
    pickSeat: (state, action) => {
      let index = state.selectedSeats.findIndex(
        (seat) => seat.maGhe == action.payload.maGhe
      );
      if (index != -1) {
        state.selectedSeats.splice(index, 1);
      } else {
        state.selectedSeats.push(action.payload);
      }
    },
  },
});

export const { loadingOn, loadingOff, setMovieBookingInfo, pickSeat } =
  movieSlice.actions;

export default movieSlice.reducer;
